import 'package:flutter/material.dart';
import 'package:form_login/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(home: LoginPage());
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _showPass = false;
  var _passwordError = null;
  var _emailError = null;
  var _emailInvalid = false;
  var _passwordInvalid = false;
  bool _inputValid = false;

  var emailError = "đuôi email: ...@dehasoft.com";
  var passwordError = "mat khau phai tu 6 ky tu tro len";
  var emailAdmin = 'dungnt@dehasoft.com';
  var passAdmin = '123456';
  var errorLogin = "email hoac mat khau khong chinh xac";

  //text sho error
  var _error = null;
  var _errorEmail = null;

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
        constraints: BoxConstraints.expand(),
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Container(
                  width: 70,
                  height: 70,
                  padding: EdgeInsets.all(15),
                  decoration:
                      BoxDecoration(shape: BoxShape.circle, color: Colors.grey),
                  child: FlutterLogo()),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 30),
              child: Text(
                "Hello\nHow are you!",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 30),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Column(
                children: [
                  TextField(
                    onChanged: (val) {
                      checkEmailInput();
                      checkShowButton();
                    },
                    controller: _emailController,
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                    decoration: InputDecoration(
                        labelText: "EMAIL",
                        labelStyle:
                            TextStyle(color: Colors.grey, fontSize: 15)),
                  )
                ],
              ),
            ),
            _error != null
                ? Text(
                    _error,
                    style: TextStyle(color: Colors.amber),
                  )
                : SizedBox(),
            _emailError != null
                ? Text(
                    _emailError,
                    style: TextStyle(color: Colors.amber),
                  )
                : SizedBox(),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: Stack(
                  alignment: AlignmentDirectional.centerEnd,
                  children: <Widget>[
                    TextField(
                      onChanged: (val) {
                        checkPasswordInput();
                        //kiem tra 2 cai
                        checkShowButton();
                      },
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      controller: _passwordController,
                      obscureText: !_showPass,
                      decoration: InputDecoration(
                          labelText: "PASSWORD",
                          // errorText: _passwordInvalid ? passwordError : null,
                          labelStyle:
                              TextStyle(color: Colors.grey, fontSize: 15)),
                    ),
                    GestureDetector(
                      onTap: onClickShow,
                      child: Text(
                        _showPass ? "HIDE" : "SHOW",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 15,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ]),
            ),
            _error != null
                ? Text(
                    _error,
                    style: TextStyle(color: Colors.amber),
                  )
                : SizedBox(),
            _passwordError != null
                ? Text(
                    _passwordError,
                    style: TextStyle(color: Colors.amber),
                  )
                : SizedBox(),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
              child: SizedBox(
                width: double.infinity,
                height: 50,
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)))),
                  child: Text('SIGN IN',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold)),
                  onPressed: _inputValid ? onSignInClick : null,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // start show ỏ hidden password
  void onClickShow() {
    setState(() {
      _showPass = !_showPass;
    });
  }

  Widget gotoHome(BuildContext context) {
    return HomePage();
  }

  void checkEmailInput() {
    if (!isValidEmail(_emailController.text)) {
      setState(() {
        _emailError = emailError;
        _error = null;
      });
    } else {
      setState(() {
        _emailError = null;
        _error = null;
      });
    }
  }

  //hien thi khi chua nhap du ky tu
  void checkPasswordInput() {
    if (_passwordController.text.length < 6) {
      setState(() {
        _passwordError = passwordError;
      });
    } else {
      setState(() {
        _passwordError = null;
        _error = null;
      });
    }
  }

  //kiem tra dl hợp lệ thì show button
  void checkShowButton() {
    setState(() {
      if (_passwordController.text.length < 6 ||
          !isValidEmail(_emailController.text)) {
        _inputValid = false;
      } else {
        _inputValid = true;
      }
    });
  }

  void onSignInClick() {
    setState(() {
      if (_emailController.text == emailAdmin) {
        _emailInvalid = true;
      } else {
        _emailInvalid = false;
        _error = errorLogin;
      }

      if (_passwordController.text == passAdmin) {
        _passwordInvalid = true;
      } else {
        _passwordInvalid = false;
        _error = errorLogin;
      }

      if (_emailInvalid && _passwordInvalid) {
        Navigator.push(context, MaterialPageRoute(builder: gotoHome));
      }
    });
  }

  bool checkEmail(String email) {
    return email == emailAdmin;
  }

  bool checkPassword(String password) {
    return password == passAdmin;
  }

  bool isValidEmail(String email) {
    RegExp regex = RegExp(r'^[\w-\.]+@dehasoft\.com$');
    return regex.hasMatch(email);
  }
}

// tao 1 text la null cho ca password va email, neu sai thi hien thi state lai loi la " email hoac mat khau khong chinh xac" va khi chane thi se an loi di
